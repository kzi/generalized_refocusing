# README #

The repo contains a CoQ library with a procedure
for translating reduction semantics to abstract machines,
together with a proof correctness of the translation.

Remarks and bugs can be reported to

kzi@cs.uni.wroc.pl